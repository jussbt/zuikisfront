import React, {Component} from 'react';
import MenuComponent from "./menuComponents/MenuComponent";
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import MainView from "./MainView";
import ContactComponent from "./contact/ContactComponent";



class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <MenuComponent/>
                        <Switch>
                            <Route exact path="/" render={props => <MainView {...props} />}/>
                            <Route exact path="/susisiekti" render={props => <ContactComponent {...props} />}/>
                            {/*<Route exact path="/prisijungti" render={props => <LoginComponent {...props} />}/>*/}
                            {/*<Route exact path="/registracija" render={props => <RegistrationComponent {...props} />}/>*/}
                            {/*<Route exact path="/pranesti" render={props => <RegisterInspectionComponent {...props} />}/>*/}
                        </Switch>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
