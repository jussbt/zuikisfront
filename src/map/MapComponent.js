import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import {compose, withProps} from "recompose"
import {GoogleMap, Marker, withScriptjs, withGoogleMap, InfoWindow} from "react-google-maps"
import styles from '../styles/MapStyles'

const MapComponent = compose(
    withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDOmD9DtnN06tG4uF-DYetkqMYWD-2ACrg",
        loadingElement: <div style={styles.loadingElement}/>,
        containerElement: <div style={styles.containerElement}/>,
        mapElement: <div style={styles.mapElement}/>,

    }),
    withScriptjs,
    withGoogleMap,
)((props) =>
    <GoogleMap
        onClick={(e) => alert()}
        defaultZoom={13}
        defaultCenter={{lat: 54.687157, lng: 25.279652}}>
        {<Marker position={{lat: 54.687157, lng: 25.279652}} onDragEnd={() => alert('aa')} draggable={true} title={"Parodyk kur kontrolė!"} label="Registruok!"/>}
    </GoogleMap>
);

export default MapComponent;

