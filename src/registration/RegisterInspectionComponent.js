import React, {Component} from 'react';
import FlexView from 'react-flexview/lib';

const RegisterInspectionComponent = (props) => {
    return (
        <FlexView column={true} hAlignContent={'center'}>
            {cards}
            <FlexView hAlignContent={'center'} marginTop={20}>
                <div className="ui middle aligned list">
                    <form className="ui form">
                        <label>Miestas</label>
                        <label>Telefonas</label>
                        <input type="text" placeholder="Telefono numeris" maxLength={12}/>
                        <div className="field">
                            <label>Elektroninis paštas</label>
                            <input type="text" placeholder="Elektroninis paštas"/>
                            <label>Žinutė</label>
                            <textarea maxLength={255}/>
                        </div>
                        <FlexView hAlignContent={'center'}>
                            <button className="positive ui button">Parašyti</button>
                        </FlexView>
                    </form>
                </div>
            </FlexView>
        </FlexView>
    )
}
const cards = (
    <div className="ui cards ">
        <div className="card">
            <div className="content">
                <div className="center aligned header">Pranešdami gaunate taškus!</div>
                <div className="center aligned description">
                    <p>Už taškus galėsite gauti bilietėlių arba mėnėsinį bilietą ! </p>
                </div>
            </div>
        </div>
    </div>
)


export default RegisterInspectionComponent;