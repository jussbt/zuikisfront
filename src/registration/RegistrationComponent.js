import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import FlexView from 'react-flexview/lib';

const RegistrationComponent = (props) => {
    return (
        <FlexView hAlignContent={'center'} marginTop={100}>
            <div className="ui middle aligned list">
                <form className="ui form">
                    <div className="field">
                        <label>Jūsų elektroninis paštas</label>
                        <input type="text" placeholder="El. paštas" maxLength={30}/>
                    </div>
                    <div className="field">
                        <label>Jūsų prisijungimo vardas</label>
                        <input type="text" placeholder="Slapyvardis" maxLength={10}/>
                    </div>
                    <div className="field">
                        <label>Slaptažodis</label>
                        <input type="password" placeholder="Slaptažodis" maxLength={12}/>
                    </div>
                    <div className="field">
                        <label>Pakartokite slaptažodį</label>
                        <input type="password" placeholder="Slaptažodis" maxLength={12}/>
                    </div>
                    <FlexView hAlignContent={'center'}>
                        <button className="positive ui button">Registruotis</button>
                    </FlexView>
                </form>
            </div>
        </FlexView>

    )
};
export default RegistrationComponent;