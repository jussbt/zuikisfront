import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import FlexView from 'react-flexview/lib';
import justasFoto from '../pictures/justasFoto.jpg'

const ContactComponent = (props) => {
    return (
        <FlexView column={true}>
            <FlexView hAlignContent={'center'}>
                {cards}
            </FlexView>
            <FlexView hAlignContent={'center'} marginTop={20}>
                <div className="ui middle aligned list">
                    <form className="ui form">
                        <label>Jūsų Vardas</label>
                        <input type="text" placeholder="Vardas" maxLength={15}/>
                        <label>Telefonas</label>
                        <input type="text" placeholder="Telefono numeris" maxLength={12}/>
                        <div className="field">
                            <label>Elektroninis paštas</label>
                            <input type="text" placeholder="Elektroninis paštas"/>
                            <label>Žinutė</label>
                            <textarea maxLength={255}/>
                        </div>
                        <FlexView hAlignContent={'center'}>
                            <button className="positive ui button">Parašyti</button>
                        </FlexView>
                    </form>
                </div>
            </FlexView>
        </FlexView>
    )
};

const cards = (
    <div className="ui cards ">
        <div className="card">
            <div className="content">
                <div className="center aligned header">Justas Subatavičius</div>
                <div className="center aligned description">
                    <p>Įdėjos labai laukiamos! <br/>
                        Stengiuosi įgyvendinti kas yra įmanoma.</p>
                </div>
            </div>
            <div className="extra content">
                <div className="center aligned author">
                    <img className="ui avatar image" src={justasFoto} alt={'justas'}/>Programuotojas
                </div>
            </div>
        </div>
        <FlexView className="card" column={true} marginLeft={50}>
            <div className="content">
                <div className="center aligned header">Justas Subatavičius</div>
                <div className="center aligned description">
                    <p>Pranešdami apie klaidas - jūs prisidėdate prie šio projekto įdėjos</p>
                </div>
            </div>
            <div className="extra content">
                <div className="center aligned author">
                    <img className="ui avatar image" src={justasFoto} alt={'justas'}/>Testuotojas
                </div>
            </div>
        </FlexView>
        <FlexView className="card" column={true} marginLeft={50}>
            <div className="content">
                <div className="center aligned header">Justas Subatavičius</div>
                <div className="center aligned description">
                    <p>Duomenys yra koduojami!<br/>
                        Užtikriname saugumą!</p>
                </div>
            </div>
            <div className="extra content">
                <div className="extra content">
                    <div className="center aligned author">
                        <img className="ui avatar image" src={justasFoto} alt={'justas'}/>Projekto vadovas
                    </div>
                </div>
            </div>
        </FlexView>
    </div>
)


export default ContactComponent;
