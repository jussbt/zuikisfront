import React from 'react';
import FlexView from 'react-flexview';
import {Link} from 'react-router-dom';
import {Button} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

const MenuComponent = () => {
    return (
        <FlexView className="ui bottom menu">
            <Link to="">
                <Button className="item active">
                    <i className="home icon"></i>
                    Pagrindinis
                </Button>
            </Link>
            {/*<Link to="/prisijungti">*/}
                {/*<Button className="item">*/}
                    {/*<i className="user circle outline icon"></i>*/}
                    {/*Prisijungti*/}
                {/*</Button>*/}
            {/*</Link>*/}
            {/*<Link to="/registracija">*/}
                {/*<Button className="item">*/}
                    {/*<i className="address card outline icon"></i>*/}
                    {/*Registracija*/}
                {/*</Button>*/}
            {/*</Link>*/}
            <Link to="/susisiekti">
                <Button className="item">
                    <i className="bell outline icon"></i>
                    Susisiekti
                </Button>
            </Link>
            {/*<Link to="/pranesti">*/}
                {/*<Button className="item">*/}
                    {/*<i className="users icon"></i>*/}
                    {/*<b>Pranešti apie kontrolę</b>*/}
                {/*</Button>*/}
            {/*</Link>*/}
            <FlexView className='right item button'>
                <button className="ui facebook button">
                    <i className="facebook icon"></i>
                    Facebook
                </button>
            </FlexView>
        </FlexView>
    );
};
export default MenuComponent;
