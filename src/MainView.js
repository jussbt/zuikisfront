import React from 'react';
import MapComponent from "./map/MapComponent";
import FlexView from 'react-flexview/lib';


class MainView extends React.Component {
    render() {
        return (
            <FlexView column={true} width={'100%'}>
                <MapComponent />
            </FlexView>
        )
    }
}

export default MainView;