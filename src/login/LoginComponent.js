import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import FlexView from 'react-flexview/lib';

const LoginComponent = (props) => {
    return (
        <FlexView hAlignContent={'center'} marginTop={100}>
            <div className="ui middle aligned list">
                <form className="ui form">
                    <div className="field">
                        <label>Jūsų elektroninis paštas</label>
                        <input type="text" placeholder="El. paštas" maxLength={30}/>
                        <label>Slaptažodis</label>
                        <input type="password" placeholder="Slaptažodis"/>
                    </div>
                    <FlexView hAlignContent={'center'}>
                        <button className="positive ui button">Prisijungti</button>
                        <button className="negative ui button">Priminti slaptažodį</button>
                    </FlexView>
                </form>
            </div>
        </FlexView>

    )
};
export default LoginComponent;