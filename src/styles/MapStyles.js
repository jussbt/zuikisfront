const styles={
    mapElement:{
        height:'200%',
        width:'100%'
    },
    containerElement:{
        height: `450px`
    },
    loadingElement:{
        height: `100%`
    }
}
export default styles;